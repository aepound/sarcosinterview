/* vectorTest.c */
#include<stdio.h>
#include<float.h>
#include "matrix.h"



int main(int argc, char *argv[]) {

  printf("Demonstrating transpose Mat33...\n");


  struct Mat33 M1;
  M1.col[0].x = 1;
  M1.col[0].y = 4;
  M1.col[0].z = 7;
  M1.col[1].x = 2;
  M1.col[1].y = 5;
  M1.col[1].z = 8;
  M1.col[2].x = 3;
  M1.col[2].y = 6;
  M1.col[2].z = 9;

  printMat33(&M1);
  printf("Transpose:\n");
  transposeMat33(&M1);
  printMat33(&M1);

  printf("Demo successful...\n");
  return 0;
}
