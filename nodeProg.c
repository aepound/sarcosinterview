/* vectorTest.c */
#include<stdio.h>
#include<float.h>
#include "node.h"



int main(int argc, char *argv[]) {

  printf("Exercising Node Pretty Printing...\n");

  struct Mat33 M1;
  struct Mat33 M2;
  struct Mat33 M3;
  M1.col[0].x = 1;
  M2.col[1].y = 1;
  M3.col[2].z = 1;


  struct Node N1,N2,N3;
  N1.data = &M1;

  N1.child =&N2;
  N2.data = &M2;

  printf("---------------------\n");
  N2.child =&N3;
  N3.data = &M3;
  printDescendents(&N1);


  printf("Exercise successful...\n");
  return 0;
}
