
This is a repo for the Sarcos Coding Challenge part of the interview.



Part 7. Discuss the stack and heap memory usage in Part 6.

I chose to do a looping method, instead of a recursive implementation. 
This meant that all of the allocation was performed in the single function. 
The stack stayed relatively flat, except for the function frames for the 
various calls into the deep copy routines. The pointers curr_in and curr_out
were also allocated on the stack, as they are variables in the deepCopyNode() 
function.  All of the dynamically allocated variables were allocated by calloc 
on the heap. Thus, the heap was increased in size by sizeof(Mat33)*depthOfList.

