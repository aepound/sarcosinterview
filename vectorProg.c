/* vectorTest.c */
#include<stdio.h>
#include<float.h>
#include "vector.h"



int main(int argc, char *argv[]) {

  printf("Vec3 dot product program.\n");

  /* Need to test the dot product. 
    > Essentially to be the dot product: The algorithm is each component is multiplied, then summed.
    > I don't think I'll work through the full linearity constraints that a "true" dot product will need.
      I'll just start with a few easy tests... */
  struct Vec3 v1;
  struct Vec3 v2;
  double result1;

  v1.x = 1;
  v1.y = 2;
  v1.z = 3;

  printf("v1: ");
  printVec3(v1);

  v2.x = 4;
  v2.y = 5;
  v2.z = 6;

  printf("v2: ");
  printVec3(v2);

  result1 = dot(v1,v2);
  printf("dot(v1,v2) = %f\n", result1);
  return 0;
}
