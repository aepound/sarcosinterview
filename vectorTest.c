/* vectorTest.c */
#include<stdio.h>
#include<float.h>
#include "vector.h"



int main(int argc, char *argv[]) {

  printf("Testing Vec3...\n");

  /* Need to test the dot product. 
    > Essentially to be the dot product: The algorithm is each component is multiplied, then summed.
    > I don't think I'll work through the full linearity constraints that a "true" dot product will need.
      I'll just start with a few easy tests... */
  struct Vec3 v1;
  struct Vec3 v2;
  double result1;

  v1.x = 1;
  v1.y = 0;
  v1.z = 0;

  printf("v1: ");
  printVec3(v1);

  v2.x = 1;
  v2.y = 0;
  v2.z = 0;
 
  printf("v2: ");
  printVec3(v2);

  result1 = dot(v1,v2);
  if(result1 - 1.0 > FLT_EPSILON){
    printf("FAILED: dot() 1\n"); 
    return 1;
  } 
  
  v1.y = 1;
  result1 = dot(v1,v2);
  if(result1 - 1.0 > FLT_EPSILON){
    printf("FAILED: dot() 2\n"); 
    return 1;
  } 

  v2.y = 1;
  result1 = dot(v1,v2);
  if(result1 - 2.0 > FLT_EPSILON){
    printf("FAILED: dot() 3\n"); 
    return 1;
  } 

  v1.z = 1;
  result1 = dot(v1,v2);
  if(result1 - 2.0 > FLT_EPSILON){
    printf("FAILED: dot() 4\n"); 
    return 1;
  } 

  v2.z = 3;
  result1 = dot(v1,v2);
  if(result1 - 5.0 > FLT_EPSILON){
    printf("FAILED: dot() 5\n"); 
    return 1;
  } 


  printf("Testing successful...\n");
  return 0;
}
