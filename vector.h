#ifndef VECTOR_H
#define VECTOR_H
#include<stdio.h>

struct Vec3 {
  double x, y, z;
};

double dot(struct Vec3 v1, struct Vec3 v2)
{
  return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

void printVec3(struct Vec3 v){
/* Pretty print a 3-vector on a single line. */
printf(" (%f %f %f) \n",v.x, v.y, v.z);
}

#endif /* VECTOR_H */
