/* vectorTest.c */
#include<stdio.h>
#include<float.h>
#include "matrix.h"



int main(int argc, char *argv[]) {

  printf("Testing Vec3...\n");

  /* Need to test the dot product. 
    > Essentially to be the dot product: The algorithm is each component is multiplied, then summed.
    > I don't think I'll work through the full linearity constraints that a "true" dot product will need.
      I'll just start with a few easy tests... */

  struct Mat33 M1;
  M1.col[2].x = 2;
  M1.col[1].x = 3.5;
  M1.col[2].y = -5;

  printMat33(&M1);
  printf("Transpose:\n");
  transposeMat33(&M1);
  printMat33(&M1);

  /*
  if(result1 - 5.0 > FLT_EPSILON){
    printf("FAILED: dot() 5\n"); 
    return 1;
  } 

 */
  printf("Testing successful...\n");
  return 0;
}
