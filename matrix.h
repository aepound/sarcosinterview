#ifndef MATRIX_H
#define MATRIX_H
#include<stdio.h>
#include "vector.h"


struct Mat33 {
  struct Vec3 col[3];
};

void swap(double *a, double *b){
double tmp = (*a);
(*a) = (*b);
(*b) = tmp;
}

void transposeMat33(struct Mat33* M){

/* Need to swap the upper right with the lower left elements...*/
swap(&(M->col[0].y), &(M->col[1].x));
swap(&(M->col[0].z), &(M->col[2].x));
swap(&(M->col[1].z), &(M->col[2].y));
}

void printMat33(struct Mat33* m){
/* Pretty print the Matrix. */
/* Because the Vec3's are columns, we need to transpose 
 before using the printVec3() function to pretty print the 
 Mat33... */
transposeMat33(m);
printVec3(m->col[0]);
printVec3(m->col[1]);
printVec3(m->col[2]);
transposeMat33(m);
}

#endif /* MATRIX_H */
