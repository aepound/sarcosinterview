
CC=gcc
CFLAGS=-I.

all: vectorTest vectorProg matrixTest matrixProg nodeTest nodeProg deepCopyTest


vectorTest: vectorTest.o vector.h
	$(CC) -o vectorTest vectorTest.o

vectorProg: vectorProg.o vector.h
	$(CC) -o vectorProg vectorProg.o

matrix.h: vector.h

matrixTest: matrixTest.o matrix.h
	$(CC) -o matrixTest matrixTest.o

matrixProg: matrixProg.o matrix.h
	$(CC) -o matrixProg matrixProg.o

node.h: matrix.h

nodeTest: nodeTest.o node.h
	$(CC) -o nodeTest nodeTest.o

nodeProg: nodeProg.o node.h
	$(CC) -o nodeProg nodeProg.o

deepCopyTest: deepCopyTest.o node.h
	$(CC) -o deepCopyTest deepCopyTest.o 
