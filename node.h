#ifndef NODE_H
#define NODE_H
#include<stdio.h>
#include<stdlib.h>
#include "matrix.h"



struct Node {
  struct Mat33* data;
  struct Node* child;
};


void printDescendents(struct Node* N){
  if(!N){
    printf("End of Nodes..\n");
    return;
  }
  else{
    printf("\n");
  }

  printMat33(N->data);
  if(N->child){
    printDescendents(N->child);
  }
}

void deepCopyVec3(struct Vec3*in, struct Vec3* out){
  out->x = in->x;
  out->y = in->y;
  out->z = in->z;
}


void deepCopyMat33(struct Mat33* in, struct Mat33* out){
  deepCopyVec3(&(in->col[0]),&(out->col[0]));
  deepCopyVec3(&(in->col[1]),&(out->col[1]));
  deepCopyVec3(&(in->col[2]),&(out->col[2]));
}


void deepCopyNode(struct Node* in, struct Node* out){
  /* In order to be a true Deep copy, we need to traverse
    to all children and make them in the new copy.*/

  /* First: guard against in == out*/
  if(in == out)
    return;

  struct Node* curr_in, *curr_out;
  curr_in = in;
  curr_out = out;

  while(curr_in){
    if(curr_in->data){
      curr_out->data = (struct Mat33*)calloc(1,sizeof(struct Mat33));
      deepCopyMat33(curr_in->data,curr_out->data);
    }
    if(curr_in->child && !curr_out->child){
      curr_out->child = (struct Node*)calloc(1,sizeof(struct Node));
    }
    curr_in = curr_in->child;
    curr_out = curr_out->child;
  }

}


#endif /* NODE_H */
